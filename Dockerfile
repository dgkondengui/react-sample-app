ARG REACT_VERSION=17.0.2
ARG NODE_VERSION=12.2.0
ARG NGINX_VERSION=1.13.12-alpine

FROM node:$NODE_VERSION as build-stage

ARG REACT_APP_API_URL
ENV REACT_APP_API_URL $REACT_APP_API_URL

# Stage 1: Install dependencies
WORKDIR /app
COPY package*.json ./
RUN npm install

# Stage 2: Build static assets

# Optinally install a Reactjs version instead of using default one
# We can install and update project to use Reactjs specific version according to the Build Arg
RUN npm install react@${REACT_VERSION}

COPY . .
RUN npm run build

# Stage 3: Build final image
# Have Nginx to serve static assets generated from previous Stage
ARG NGINX_VERSION
FROM nginx:$NGINX_VERSION as production-stage
RUN mkdir -p /usr/share/nginx/html
RUN mkdir -p /usr/share/nginx/html/acme

COPY --from=build-stage /app/build /usr/share/nginx/html
COPY vhost.conf /etc/nginx/conf.d/
COPY mime.types /etc/nginx/mime.types

RUN rm /etc/nginx/conf.d/default.conf

# Configure timestamp
RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime

# Create file for acme challenge
RUN echo "acme-test" > /usr/share/nginx/html/acme/index.html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
